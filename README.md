# dotfiles
my environment

- MacBook Pro 2023
- CPU: M2
- OS: Ventura 13.4.1

```shell
uname -a
Darwin mitsus-M2-MBP.local 22.4.0 Darwin Kernel Version 22.4.0: Mon Mar  6 20:59:58 PST 2023; root:xnu-8796.101.5~3/RELEASE_ARM64_T6020 arm64
```
Ubuntu 22.10:  
```shell
❯ uname -a
Linux rpi4-4g 5.19.0-1009-raspi #16-Ubuntu SMP PREEMPT Thu Nov 24 13:38:20 UTC 2022 aarch64 aarch64 aarch64 GNU/Linux
```

## Raspberry Pi OS
```shell
Linux mitsu-pi4 6.1.0-rpi7-rpi-v8 #1 SMP PREEMPT Debian 1:6.1.63-1+rpt1 (2023-11-24) aarch64 GNU/Linux
```

## requirements

- zsh
  - zinit
  - Powerlevel10k
  - [direnv](https://direnv.net/)
  - dotenv
- Node.js
  - nvm
- Python
  - [pyenv](https://github.com/pyenv/pyenv)
  - [pipenv](https://github.com/pypa/pipenv)
- AWS
  - [aws cli](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html)
  - [aws sam](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/what-is-sam.html)
    - [podman](https://podman.io)

optional

- [PyCharm Community Edition](https://www.jetbrains.com/pycharm/)
- [Fira Code](https://github.com/tonsky/FiraCode)
  - free monospaced font with programming ligatures

## remove something(option)

### Ubuntu
Ubuntu desktop:  
```shell
sudo apt remove --purge thunderbird* libreoffice* gnome-sudoku gnome-mines gnome-mahjongg aisleriot
```
## install tools

Ubuntu desktop:
```shell
sudo apt install gnome-tweaks chromium-browser
sudo apt install zsh curl wget httpie git openssh-server
sudo apt install nautilus-share
sudo apt install direnv net-tools
```

### Raspberry OS
```shell
sudo apt remove --purge geany vlc thonny rpi-imager
```


## change zsh
Change login shell to zsh from bash.

```
> which zsh
/usr/bin/zsh
```

```
chsh -s /usr/bin/zsh
```

## fonts install
Ubuntu desktop:  
```shell
sudo apt install fonts-jetbrains-mono fonts-jetbrains-mono-web -y
```
mac OS:
```shell
brew install font-jetbrains-mono font-jetbrains-mono-nerd-font
```

## install zinit
[GitHub zinit](https://github.com/zdharma-continuum/zinit)  
[Install Automatic](https://github.com/zdharma-continuum/zinit#install)

### edit .zshrc

c.f. [【Zsh】大改造ターミナル環境【Zinit】](https://qiita.com/obake_fe/items/c2edf65de684f026c59c#zinit)

#### Completions
```shell
# zsh-completions
zinit ice wait'0'; zinit light zsh-users/zsh-completions
autoload -Uz compinit && compinit
## 補完で小文字でも大文字でもマッチさせる
# zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
## 補完候補を一覧表示したとき、Tab や矢印で選択できるようにする
zstyle ':completion:*:default' menu select=1
```
#### Syntax Highlighting
```shell
# zsh-syntax-highlighting
zinit light zsh-users/zsh-syntax-highlighting

```
#### Auto Suggestions
```shell
# zsh-autosuggestions
zinit light zsh-users/zsh-autosuggestions
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=244"
```
#### command history
^r で、コマンド履歴の表示と選択  
```shell
# history-search-multi-word
zinit light zdharma/history-search-multi-word
```
#### Git branch display and checkout
```shell
# git branch display and change
zinit light mollifier/anyframe
bindkey '^xb' anyframe-widget-checkout-git-branch
autoload -Uz chpwd_recent_dirs cdr add-zsh-hook
add-zsh-hook chpwd chws_recent_dirs
````
#### シンタックス・ハイライト
```shell
# Powerlevel10k
zinit ice depth=1; zinit light romkatv/powerlevel10k
```

# aliases

```shell
case "${OSTYPE}" in
  darwin*)
    alias ls="ls -G"
    alias ll="ls -lG"
    alias la="ls -laG"
    alias python="python3"
    alias docker="podman"
    ;;
  linux*)
    alias ls="ls --color"
    alias ll="ls -l --color"
    alias la="ls -la --color"
  ;;
esac
````

## install Powerlevel10k

[GitHub Powerlevel10k](https://github.com/romkatv/powerlevel10k)

1. add fonts
1. change font your terminal
1. restart terminal

### install Powerlevel10k
add next your `.zshrc` at the last line.

```shell
# Powerlevel10k
zinit ice depth=1; zinit light romkatv/powerlevel10k
```

c.f. [Powerlevel10k > Installation > Zinit](https://github.com/romkatv/powerlevel10k#zinit)

## for the ssh

add `ssh-agent` in your `.zshrc`

```shell
# SSH
case "${OSTYPE}" in
  darwin*)
    ;;
  linux*)
    eval `ssh-agent` > /dev/null 2>&1
    ;;
esac
```

and create `.zlogout`

## for the Node.js

install and settings [nvm](https://github.com/nvm-sh/nvm)

> nvm is a version manager for node.js, designed to be installed per-user, and invoked per-shell.

```shell
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
```

## for the Python

### pyenv

install and settings [pyenv](https://github.com/pyenv/pyenv)

> pyenv lets you easily switch between multiple versions of Python. It's simple, unobtrusive, and follows the UNIX tradition of single-purpose tools that do one thing well.

```shell
curl https://pyenv.run | bash
```

for install Python

Ubuntu:  
```shell
sudo apt update; sudo apt install build-essential libssl-dev zlib1g-dev \
libbz2-dev libreadline-dev libsqlite3-dev curl llvm \
libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev
```

macOS:  
```shell
brew install openssl readline sqlite3 xz zlib tcl-tk
```

install Python 3.10 and 3.9

example:  
```shell
pyenv install 3.10.9
pyenv install 3.9.16
```

c.f. Confirm installation version  
```shell
pyenv install --list | grep 3.10
```

### pipenv

> Pipenv is a tool that aims to bring the best of all packaging worlds (bundler, composer, npm, cargo, yarn, etc.) to the Python world.

Ubuntu:
```shell
sudo apt install pipenv
```

macOS:  
```
brew install pipenv
```

### spyder

> Spyder is a free and open source scientific environment written in Python, for Python, and designed by and for scientists, engineers and data analysts.

Ubuntu:  
```shell
sudo apt install spyder
```

macOS: not installed  
```shell
brew install spyder
```

## direnv

install and settings [direnv](https://direnv.net/)

> direnv is an extension for your shell. It augments existing shells with a new feature that can load and unload environment variables depending on the current directory.

Ubuntu:  
```sheell
sudo apt install direnv
```

macOS:  
```shell
brew install direnv
```

c.f.  
[direnvを使うときは環境変数を.envrcに書くより.envに書いた方が使い勝手が良い](https://blog.p1ass.com/posts/direnv-dotenv/)

## for the AWS

### AWS CLI

```shell
brew install awscli
```

in my .zshrc
```shell
complete -C '/opt/homebrew/bin/aws_completer' aws
```

### AWS SAM

AWS SAM's official installation is bellow.

If you need official version.

```shell
brew install aws/tap/aws-sam-cli
```
'aws/tap/aws-sam-cli' based on Python 3.8.

I was installing `brew install aws-sam-cli`.
'aws-sam-cli' based on Python 3.11.

```shell
brew install aws-sam-cli
```

Checked in "SAM CLI"
- Build function inside a container
- Skip checking for newer container images


#### Docker Desktop
I installed [podman](https://podman.io). c.f. [Podman Installation Instructions](https://podman.io/getting-started/installation)
Because  
- [Docker Subscription and Licensing Questions](https://www.docker.com/pricing/faq/)

> Docker desktop is about to become a paid service for larger businesses on January 31st, 2022.
> ...
> What's the best option for those of us whose companies don't want to fork out the subscription fee?


[Installing Docker](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/install-docker.html#install-docker-instructions)

> Docker Desktop is officially supported, but starting with AWS SAM CLI version 1.47.0, you can use alternatives as long as they use the Docker runtime.

- [AWS re:Post](https://www.repost.aws)
  - [Docker Alternatives for SAM CLI](https://www.repost.aws/questions/QUDeslR-ntRz-A4c9apB-jCQ/docker-alternatives-for-sam-cli)

Question:  
> Hi, does anyone know of any good Docker alternatives (that use the Docker runtime) for Windows to use with the SAM CLI for local development / testing?

> I'm asking this based on the note in the SAM CLI installation page (https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install-windows.html) which I've pasted below:

> Note We officially support Docker Desktop but, starting with SAM CLI version 1.47.0, you can use alternatives as long as they use the Docker runtime.

Answer:  
> If your looking for a Docker alternative to work with containers you could consider Podman (https://podman.io/)

[Support local development on machines running Podman instead of Docker](https://github.com/aws/aws-sam-cli/issues/1668)

### podman

I installed [podman](https://podman.io) instead of [Docker Desktop](https://www.docker.com/products/docker-desktop/).

```shell
brew install podman
podman machine init --volume $HOME/Develop:$HOME/Develop
podman machine start
```

c.f.
```shell
podman machine init \
    --volume /Users:/mnt/Users \
    --volume /Volumes:/mnt/Volumes \
    --volume /private:/mnt/private \
    --volume /tmp:/mnt/tmp
```

I added bellows in my `~/.zshrc`.

```shell
export DOCKER_HOST=unix://`podman machine inspect --format '{{.ConnectionInfo.PodmanSocket.Path}}'`
alias docker="podman"
```

And I installed `docker` because **ERROR** in PyCharm `sam build` via aws-toolkit.

```shell
brew install docker
```

```shell
❯ podman machine start
Starting machine "podman-machine-default"
Waiting for VM ...
Mounting volume... /Users/mitsu/Develop:/Users/mitsu/Develop
API forwarding listening on: /Users/mitsu/.local/share/containers/podman/machine/qemu/podman.sock

The system helper service is not installed; the default Docker API socket
address can't be used by podman. If you would like to install it run the
following commands:

	sudo /opt/homebrew/Cellar/podman/4.5.1/bin/podman-mac-helper install
	podman machine stop; podman machine start

You can still connect Docker API clients by setting DOCKER_HOST using the
following command in your terminal session:

	export DOCKER_HOST='unix:///Users/mitsu/.local/share/containers/podman/machine/qemu/podman.sock'

Machine "podman-machine-default" started successfully
```

```shell
sudo /opt/homebrew/Cellar/podman/4.5.1/bin/podman-mac-helper install
podman machine stop; podman machine start
```

[Bug: local start-api breaks under Podman](https://github.com/aws/aws-sam-cli/issues/5019)  
[fix: Favor a stale image when daemon doesn't support registry query](https://github.com/aws/aws-sam-cli/pull/5020/files)  

## Giveaway

### Fira Code
[Fira Code](https://github.com/tonsky/FiraCode): free monospaced font with programming ligatures

### PyCharm
Because [Sunsetting Atom](https://github.blog/2022-06-08-sunsetting-atom/),
I'm using [PyCharm Community Edition](https://www.jetbrains.com/products/compare/?product=pycharm&product=pycharm-ce).

#### plug ins
- .env files support
- .ignore
- [AWS Toolkit for PyCharm](https://aws.amazon.com/jp/pycharm/)
- CSV Editor
- Dracula Theme
- File Watchers
- ~~Rainbow Brackets~~
- Nested Brackets Colorer

## Apple silicon virtualization

WWDC2022:  
[Create macOS or Linux virtual machines](https://developer.apple.com/videos/play/wwdc2022/10002/)  
[documentation/Virtualization](https://developer.apple.com/documentation/virtualization)
[Running GUI Linux in a virtual machine on a Mac](https://developer.apple.com/documentation/virtualization/running_gui_linux_in_a_virtual_machine_on_a_mac)

## PULSAR
> Pulsar, or sometimes referred to as Pulsar-Edit is a new image of the beloved 'Hackable Text Editor' Atom.

> After the announcement of Atom's sunset, the community came together to keep Atom alive via the longstanding fork Atom-Community.

Atom は有志たちにより [PULSAR](https://pulsar-edit.dev) として蘇っています。  
各種のプラグインも従来と同様に [Packages make Pulsar do amazing things.](https://web.pulsar-edit.dev)より利用可能ですが、...  
最も利用したい [hydrogen](https://web.pulsar-edit.dev/packages/hydrogen)

> macOS Performance: Currently performance on MacOS isn't what we hope to achieve. Often times this can be resolved by disabling the github package.

macOSのパフォーマンス：現在、MacOSでのパフォーマンスは、私たちが達成したいものではありません。多くの場合、これはgithubパッケージを無効にすることで解決できます。

## Streamlink
I'm listening [J-WAVE 81.3FM](https://www.j-wave.co.jp/) using [Streamlink](https://streamlink.github.io/).

```shell
sudo apt install streamlink python3-streamlink-doc
```

and create `~/.local/bin/streamlink-radiko.sh`.

```bash
#!/usr/bin/env bash
streamlink --player ffplay --player-args '-nodisp' --player-continuous-http radiko.jp/#!/${1} best
```

usage:

```shell
streamlink-radiko.sh live/FMJ
```
