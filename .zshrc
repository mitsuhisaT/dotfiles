# Lines configured by zsh-newuser-install
HISTFILE=~/.zsh_history
HISTSIZE=2000
SAVEHIST=2000
setopt beep nomatch
bindkey -e
# End of lines configured by zsh-newuser-install

# The following lines were added by compinstall
zstyle :compinstall filename '/home/mitsu/.zshrc'

### Added by Zinit's installer
if [[ ! -f $HOME/.local/share/zinit/zinit.git/zinit.zsh ]]; then
    print -P "%F{33} %F{220}Installing %F{33}ZDHARMA-CONTINUUM%F{220} Initiative Plugin Manager (%F{33}zdharma-continuum/zinit%F{220})…%f"
    command mkdir -p "$HOME/.local/share/zinit" && command chmod g-rwX "$HOME/.local/share/zinit"
    command git clone https://github.com/zdharma-continuum/zinit "$HOME/.local/share/zinit/zinit.git" && \
        print -P "%F{33} %F{34}Installation successful.%f%b" || \
        print -P "%F{160} The clone has failed.%f%b"
fi

source "$HOME/.local/share/zinit/zinit.git/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit

# Load a few important annexes, without Turbo
# (this is currently required for annexes)
zinit light-mode for \
    zdharma-continuum/zinit-annex-as-monitor \
    zdharma-continuum/zinit-annex-bin-gem-node \
    zdharma-continuum/zinit-annex-patch-dl \
    zdharma-continuum/zinit-annex-rust

### End of Zinit's installer chunk

# Powerlevel10k
zinit ice depth=1; zinit light romkatv/powerlevel10k

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# zsh-completions
zinit ice wait'0'; zinit light zsh-users/zsh-completions
autoload bashcompinit && bashcompinit
autoload -Uz compinit && compinit -d
case "${OSTYPE}" in
  darwin*)
    complete -C '/opt/homebrew/bin/aws_completer' aws
    ;;
  linux*)
    complete -C '/usr/local/bin/aws_completer' aws
    ;;
esac

## 補完で小文字でも大文字でもマッチさせる
# zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
## 補完候補を一覧表示したとき、Tab や矢印で選択できるようにする
zstyle ':completion:*:default' menu select=1

# zsh-syntax-highlighting
zinit light zsh-users/zsh-syntax-highlighting

# zsh-autosuggestions
zinit light zsh-users/zsh-autosuggestions
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=244"

# history-search-multi-word
zinit light zdharma/history-search-multi-word

# git branch display and change
zinit light mollifier/anyframe
bindkey '^xb' anyframe-widget-checkout-git-branch
autoload -Uz chpwd_recent_dirs cdr add-zsh-hook
add-zsh-hook chpwd chpwd_recent_dirs

# Powerlevel10k
zinit ice depth=1; zinit light romkatv/powerlevel10k

# ==> sqlite
# sqlite is keg-only, which means it was not symlinked into /usr/local,
# because macOS already provides this software and installing another version in
# parallel can cause all kinds of trouble.
# 
# If you need to have sqlite first in your PATH run:
#   echo 'export PATH="/usr/local/opt/sqlite/bin:$PATH"' >> ~/.zshrc
# export PATH="/usr/local/opt/sqlite/bin:$PATH"
# 
# For compilers to find sqlite you may need to set:
# export LDFLAGS="-L/usr/local/opt/sqlite/lib"
# export CPPFLAGS="-I/usr/local/opt/sqlite/include"
# 
# For pkg-config to find sqlite you may need to set:
# export PKG_CONFIG_PATH="/usr/local/opt/sqlite/lib/pkgconfig"

# Node.js
## nvm
case "${OSTYPE}" in
  darwin*)
    export NVM_DIR="$HOME/.nvm"
    [ -s "/opt/homebrew/opt/nvm/nvm.sh" ] && \. "/opt/homebrew/opt/nvm/nvm.sh"  # This loads nvm
    [ -s "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm" ] && \. "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion
    ;;
  linux*)
    export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
    ;;
esac

## for yarn global add
export YARN_GLOBAL="$HOME/.config/yarn/global"
export PATH="$YARN_GLOBAL/bin:$PATH"

# Python
## pyEnv
## https://github.com/pyenv/pyenv
export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"

## pipenv
## https://pipenv.pypa.io/en/latest/
export PIPENV_VENV_IN_PROJECT=true
## Sphinx
case "${OSTYPE}" in
  darwin*)
    export PATH="/opt/homebrew/opt/sphinx-doc/bin:$PATH"
    ;;
  linux*)
    ;;
esac


# Java(openjdk)
case "${OSTYPE}" in
  darwin*)
    export PATH="/opt/homebrew/opt/openjdk/bin:$PATH"
    ;;
  linux*)
    ;;
esac
# export CPPFLAGS="-I/opt/homebrew/opt/openjdk/include"

# direnv
eval "$(direnv hook zsh)"

# AWS
## CLI
autoload bashcompinit && bashcompinit
autoload -Uz comping && compinit
complete -C '/opt/homebrew/bin/aws_completer' aws
## SAM
export SAM_CLI_TELEMETRY=0

# environment, SSH and aliases
case "${OSTYPE}" in
  darwin*)
    export LDFLAGS="-L$(xcrun --show-sdk-path)/usr/lib"
    export CPPFLAGS="-I$(brew --prefix zlib)/include"
#     export SDKROOT=/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk
    alias ls="ls -G"
    alias ll="ls -lG"
    alias la="ls -laG"
    alias python="python3"
    alias docker="podman"
    ;;
  linux*)
    eval `ssh-agent` > /dev/null 2>&1
    alias ls="ls --color"
    alias ll="ls -l --color"
    alias la="ls -la --color"
    ;;
esac

# language
export LC_COLLATE=ja_JP.UTF-8
export LC_CTYPE=ja_JP.UTF-8
export LC_MESSAGES=en_US.UTF-8
export LC_CTYPE=ja_JP.UTF-8
export LC_MONETARY=ja_JP.UTF-8
export LC_TIME=en_US.UTF-8
export LANG=en_US.UTF-8
